Remove Unused Ubuntu Kernel

Purge unused Ubuntu kernels

Table of Contents

1.  Setup
2.  Script usage
3.  Configuration file
4.  How the script removes the kernels
    1.  1stcase
    2.  2ndcase
    3.  3rdtcase
5.  ToDo

Setup

I did not create a release process yet.

Meanwhile I use Git LFS to upload the package in the folder deb/
directory.

You can download the package and install it:

    sudo dpkg -i ubuntu-kernel-cleanup_<VERSION>_all.deb

if the dependencies are not met:

    sudo apt -y -f install

Script usage

you can run the script using --real-run to remove the packages or use
--dry-run to show which package would have been removed:

    ubuntu-kernel-cleanup.py --real-run

or:

    ubuntu-kernel-cleanup.py --dry-run

Configuration file

The script will look for its configuration file in the following
locations:

1.  /etc/ubuntu-kernel-cleanup.ini
2.  $HOME/.ubuntu-kernel-cleanup.ini

if both files are available, the 2nd one wins.

The file content looks as following:

    [ubuntu-kernel-cleanup]
    #
    # you can obtain a list of installed package prefixes/suffixes running: bash -c 'dpkg -l linux-*'
    #
    # for more information you can check /usr/share/doc/ubuntu-kernel-cleanup/README.txt
    #

    # If the running kernel does not belong to the list of the latest kernels,
    # obtained with the option "count", then the script will keep "count + 1" kernels.
    count = 2

    # prefix comes before the version number
    kernel_prefixes = linux-tools, linux-headers, linux-modules, linux-modules-extra, linux-image, linux-image-unsigned

    # suffix come after the version number
    kernel_suffixes = generic, aws

How the script removes the kernels

1st case

you have 4 kernels installed and you use (count = 1):

-   kernel1: it’s old, it’s the running kernel and you cannot uninstall
    this one
-   kernel2: old, intermediate kernel version, it can be uninstalled
-   kernel3: old, intermediate kernel version, it can be uninstalled
-   kernel4: the latest, it is also the running kernel and it will be
    kept

the script will remove kernel2 and kernel3

2nd case

If the running kernel does not belong to the list of the latest kernels,
obtained with the option “count”, then count + 1 will be kept.

For instance, if you use count = 2 with the following kernels installed:

-   kernel1: old, it can be uninstalled
-   kernel2: old, intermediate, but running kernel and it will be kept
-   kernel3: old, intermediate kernel version, it can be uninstalled
-   kernel4: the latest, it will be kept
-   kernel5: the latest, it will be kept

the script will remove kernel1 and kernel3

3rd case

You reboot and you are running the latest kernel.

If you use count = 2, 2 kernels will be kept:

-   kernel1: old, it can be uninstalled
-   kernel2: old, intermediate, it can be uninstalled
-   kernel3: old, intermediate kernel version, it can be uninstalled
-   kernel4: the latest, it will be kept
-   kernel5: the latest, running kernel, it will be kept

the script will remove kernel1, kernel2 and kernel3

ToDo

-   The algorithm can be improved to remove intermediate kernels,
    avoiding that in a corner case you get count + 1. If count is set to
    1 it won’t work in any case (you can’t remove the running kernel and
    you don’t want to remove the latest), but, if count is set to a
    higher number the scenario is the following:
    -   Imagine you have 3 kernels installed, count is set to 2 and you
        are running the older kernel, then you can remove the
        intermediate kernel. Right now it ends up leaving count + 1
        until reboot happens.
