#!/bin/bash
#
# the deb is tracked with git lfs:
# git lfs track "deb/**"
#
# pandoc -o README.txt --to=plain README.md
# git commit ...
#
# creating new tag:
# VERSION=0.1
# git tag -a v$VERSION -m"bump version $VERSION"
#
if ! which fpm &>/dev/null; then
    echo "please install 'fpm'"
    exit
fi

PROG_NAME="ubuntu-kernel-cleanup"
EMAIL="Massimiliano Adamo<massimiliano.adamo@geant.org>"
DESCRIPTION="Clean up unused kernel packages"
export VERSION EMAIL DESCRIPTION PROG_NAME

if ! git remote -v | grep -q $PROG_NAME; then
    echo "please run this command from the root directory of the repository"
    exit
fi

LATEST_TAG=$(git describe --tags $(git rev-list --tags --max-count=1))
PROG_VERSION=${LATEST_TAG:1}
git checkout $LATEST_TAG

chmod 0755 ${PROG_NAME}.py
chmod 0644 README.txt ubuntu-kernel-cleanup.ini

fpm -f -t deb --deb-use-file-permissions -n ${PROG_NAME} -v $PROG_VERSION --maintainer "$EMAIL"\
    --vendor "$EMAIL" -a all --description "$DESCRIPTION" --config-files etc/${PROG_NAME}.ini \
    -p deb/${PROG_NAME}_${PROG_VERSION}_all.deb -d python3-docopt -d python3-packaging -d python3-natsort \
    -s dir ${PROG_NAME}.ini=/etc/${PROG_NAME}.ini ${PROG_NAME}.py=/usr/bin/${PROG_NAME}.py \
    README.txt=usr/share/doc/${PROG_NAME}/README.txt

git checkout master
