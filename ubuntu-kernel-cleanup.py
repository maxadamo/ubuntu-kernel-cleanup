#!/usr/bin/python3
# pylint: disable=C0103
#
"""
  Remove intermediates and unused kernels from Ubuntu
  For more information, please check inside:
   - /etc/ubuntu-kernel-cleanup.ini
   - /usr/share/doc/ubuntu-kernel-cleanup/README.txt

Usage:
  ubuntu-kernel-cleanup.py (--real-run | --dry-run)
  ubuntu-kernel-cleanup.py -h | --help

Options:
  -h --help       Show this screen
  -r --real-run  Real execution
  -d --dry-run   Dry execution
"""
import re
import os
import subprocess as sp
import configparser
import platform
from natsort import natsorted
import apt
from docopt import docopt
from packaging.version import parse


def get_packages_list(pkg_match):
    """
    return a list of kernel package versions installed on the system
    """
    c_list = list(CACHE) # pylint: disable=used-before-assignment
    c_list_names = [
        element.name for element in c_list if CACHE[element].is_installed]
    c_list_installed = [element for element in c_list_names if element.startswith(
        pkg_match) and any(chr.isdigit() for chr in element)]
    c_list_installed_versions = [
        re.sub(r"(^\D+-|-\D+)", r"", x) for x in c_list_installed
    ]

    return natsorted(c_list_installed_versions)


def process_packages(version_number, prefix, execution_type):
    """process package for removal"""
    for suffix in KERNEL_SUFFIXES:  # pylint: disable=used-before-assignment
        pkg = f"{prefix}-{version_number}{suffix}"
        if execution_type == 'real':
            try:
                CACHE.update()
            except apt.cache.FetchFailedException as err:
                print(f"failed to fetch some repository: {err}")
            CACHE.open()
        try:
            CACHE[pkg].is_installed  # maybe the package does not exist
        except KeyError:
            pass
        else:
            if CACHE[pkg].is_installed:
                if execution_type == 'real':
                    remove_package(pkg)
                else:
                    print(f'Would have removed {pkg} (noop)')


def remove_package(pkg_name):
    """remove package"""
    CACHE[pkg_name].mark_delete(True, True)
    try:
        CACHE.commit()
    except Exception as err:  # pylint: disable=W0703
        print(f"Package removal failed [{str(err)}]")
    finally:
        CACHE.close()


def get_version(full_pkg_name):
    """
    get version number from the full package name
    for instance linux-generic-5.8.0-34-generic returns 5.8.0-34
    """
    return re.sub(r"(^\D+-|-\D+)", r"", full_pkg_name)


if __name__ == '__main__':

    if os.geteuid() != 0:
        print("You need root privileges to run this script")
        print("Please try again, using 'sudo'. Exiting.")
        os.sys.exit()
    INIFILE = '/etc/ubuntu-kernel-cleanup.ini'
    if not os.path.isfile(INIFILE):
        print(f"you must first create {INIFILE} and then you can run the script again.\n")
        os.sys.exit()

    CONFIG = configparser.RawConfigParser()
    CONFIG.read(INIFILE)
    try:
        KERNEL_PREFIXES = CONFIG.get(
            'ubuntu-kernel-cleanup', 'kernel_prefixes'
        ).replace(' ', '').split(',')
    except configparser.NoOptionError:
        print('could not find an array option for "kernel_prefixes". Exiting')
        os.sys.exit()

    try:
        _KERNEL_SUFFIXES = CONFIG.get(
            'ubuntu-kernel-cleanup', 'kernel_suffixes').replace(' ', '').split(',')
    except configparser.NoOptionError:
        print('could not find an array option for "kernel_suffixes". Exiting')
        os.sys.exit()

    KERNEL_SUFFIXES = [''] + [f'-{x}' for x in _KERNEL_SUFFIXES]

    try:
        COUNT = CONFIG.getint('ubuntu-kernel-cleanup', 'count')
    except configparser.NoOptionError:
        print('could not find an option for "count". Defaulting to 2')
        COUNT = 2

    if COUNT < 1:
        print("--count must greater or equal to 1")
        os.sys.exit()

    EXECUTION = 'dry'
    ARGS = docopt(__doc__)
    if ARGS.get('--real-run'):
        EXECUTION = 'real'

    running_kernel = get_version(platform.uname().release)

    # load packages cache
    CACHE = apt.cache.Cache()

    kern_list = get_packages_list('linux-image-')

    if len(kern_list) > COUNT:
        for kern_count in list(range(COUNT)):
            # do we still have kernels in the list?
            if len(kern_list) > 1:
                latest_kernel = kern_list[-1]
                if parse(running_kernel) < parse(latest_kernel):
                    # keep only items not containing the latest version
                    sanitized_list = [
                        x for x in kern_list if latest_kernel != x]
                else:
                    # we are running the latest kernel
                    sanitized_list = kern_list
                    del kern_list[-1]
        try:
            sanitized_list
        except NameError:
            pass
        else:
            # delete running kernel package from list
            purge_list = [x for x in sanitized_list if running_kernel not in x]

            # uninstall packages from list
            for kernel_pkg in KERNEL_PREFIXES:
                for kernel_version in purge_list:
                    process_packages(kernel_version, kernel_pkg, EXECUTION)

    CACHE.close()  # it returns always true... keeps closing like swivel doors :)

    # purge empty packages (it doesn't remove kernel but orphan packages)
    if EXECUTION == 'real':
        CMD = "dpkg-query -f \'${Package} ${Status}\\n\' -W linux-* | awk  \'/deinstall ok/{print $1}\' | xargs apt-get purge -y"  # pylint: disable=C0301
    else:
        CMD = "dpkg-query -f \'${Package} ${Status}\\n\' -W linux-* | awk  \'/deinstall ok/{print $1}\' | xargs apt-get purge --assume-no"  # pylint: disable=C0301

    purge = sp.Popen(CMD, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
    purge_out, _ = purge.communicate()
    print(f"\ntrying to purge empty kernel packages:\n{purge_out.decode('utf-8')}")
